# Spring Security JPA auditing

Add Spring Security context user to JPA auditing (creation date time, last modification date time, creation user id, last modification user id).

## Usage

### Maven

Import the latest `dev.valora.commons:spring-security-jpa-auditing` artifact into your pom.xml.

```
<dependency>
	<groupId>dev.valora.commons</groupId>
	<artifactId>spring-security-jpa-auditing</artifactId>
	<version>3.0.0</version>
</dependency>
```

### Entity

Extends `dev.valora.commons.springsecurityjpaauditing.model.AuditingEntity` with your JPA entities that requires auditing.

### Spring configuration

Import `dev.valora.commons.springsecurityjpaauditing.SpringSecurityJpaAuditingConfiguration` to your Spring configuration.

```
@Import(SpringSecurityJpaAuditingConfiguration.class)
```

## Requirements

* Java 17
* Spring Boot 3+